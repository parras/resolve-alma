# Installation instructions

```sh
pip3 install scipy ducc0 matplotlib
pip3 install git+https://gitlab.mpcdf.mpg.de/ift/nifty.git@NIFTy_8
pip3 install git+https://gitlab.mpcdf.mpg.de/ift/resolve.git@devel
```

# Convert data: MS to resolve-npz

```sh
python3 resolve/misc/ms2npz.py <name>.ms stokesiavg --compress
```

The already converted data can be obtained from [here](https://cloud.philiar.de/index.php/s/pgBK7NsdHatMDr7).
